from __future__ import division,print_function,absolute_import

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import utils as ut
from sklearn import preprocessing
#matplotlib inline
from BismillahTA import AutoEncoder

learning_rate = 0.01
training_epochs = 20000
batch_size = 256
display_step = 1
examples_to_show = 10

#Network Parameters
n_input = 250
n_hidden_1 = 120
n_hidden_2 = 60
n_hidden_3 = 30
n_hidden_4 = 15
dataStart = 2000
dataEnd = 10000
datasetraw = ut.readSignal("dataset/100.csv")
datasetraw_ = ut.normalisasi(datasetraw)
dataset_ = ut.getSample(datasetraw_,160,89)
dataset = dataset_

weights = {
    'encoder_h1': tf.Variable(tf.random_normal([n_input,n_hidden_1])),
    'encoder_h2': tf.Variable(tf.random_normal([n_hidden_1,n_hidden_2])),
    'encoder_h3': tf.Variable(tf.random_normal([n_hidden_2,n_hidden_3])),
    'encoder_h4': tf.Variable(tf.random_normal([n_hidden_3,n_hidden_4])),
    
    'decoder_h4': tf.Variable(tf.random_normal([n_hidden_4, n_hidden_3])),
    'decoder_h3': tf.Variable(tf.random_normal([n_hidden_3, n_hidden_2])),
    'decoder_h2': tf.Variable(tf.random_normal([n_hidden_2, n_hidden_1])),
    'decoder_h1': tf.Variable(tf.random_normal([n_hidden_1, n_input])),
}

biases = {
    'encoder_b1': tf.Variable(tf.random_normal([n_hidden_1])),
    'encoder_b2': tf.Variable(tf.random_normal([n_hidden_2])),
    'encoder_b3': tf.Variable(tf.random_normal([n_hidden_3])),
    'encoder_b4': tf.Variable(tf.random_normal([n_hidden_4])),
               
    'decoder_b4': tf.Variable(tf.random_normal([n_hidden_3])),
    'decoder_b3': tf.Variable(tf.random_normal([n_hidden_2])),
    'decoder_b2': tf.Variable(tf.random_normal([n_hidden_1])),
    'decoder_b1': tf.Variable(tf.random_normal([n_input])),
}

x = tf.placeholder("float",[None,n_input])
y_true = tf.placeholder("float",[None,n_input])

#Noising
def addNoise(input):
    #Noising dengan cara membuat 0 index tertentu secara random
    noise = np.random.choice([0,1], size=np.size(input), p=[1/4, 3/4])
    return input*noise

#Encoders
def encoder(input,w,b):
    encoded = tf.nn.sigmoid(tf.add(tf.matmul(addNoise(input),w),b))
    return encoded

#Decoders
def decoder(output,w,b):
    decoded = tf.nn.sigmoid(tf.add(tf.matmul(output,w),b))
    return decoded

def optimize(mse_target):
    c = 1
    epoch = 1
    while(c>=mse_target):
        feed_dict_train = {x: dataset, y_true: dataset}
        _, c = session.run([optimizer, cost], feed_dict=feed_dict_train)
        print(epoch,"MSE = ",c)
        epoch=epoch+1


#Construct Model
encoder_op = encoder(x,weights['encoder_h1'],biases['encoder_b1'])
decoder_op = decoder(encoder_op,weights['decoder_h1'],biases['decoder_b1'])

#Prediction
x_hat = encoder_op
y_pred = decoder_op

#Loss,optimizer, mse
cost = tf.reduce_mean(tf.pow(y_true - y_pred,2))
optimizer = tf.train.RMSPropOptimizer(learning_rate).minimize(cost)

#Main
session = tf.Session()
session.run(tf.global_variables_initializer())
optimize(0.01)

#ut.plotSignal(dataset[1])
#ut.plotSignal(encode_decode[1])
#ut.plt.show()