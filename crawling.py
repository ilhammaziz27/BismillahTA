import sys
import os
from numpy import NaN, Inf, arange, isscalar,asarray,array
import numpy as np
import utils as ut
import csv
import pandas as pd

def getFiles(dir):
    directory = os.path.join(dir)
    for root,dirs,files in os.walk(directory):
        for f in files:
            if f.endswith(".csv"):
                with open(dir+"/Anotasi/"+f) as file:
                    reader = csv.reader(file, delimiter=',')
                    anotasi = []
                    for data in reader:
                        anotasi.append(data)

                aha = anotasi[1:]
                datasetraw = ut.readSignal(dir+"/"+f)

                print(f)
                newdataset = []
                for z in aha:
                    sample = ut.getSampleByRPeakDefined(datasetraw, int(z[1]), 149, 100)

                    if(sample!=[]):
                        sample_ = np.append(sample,[z[2]])
                        newdataset.append(sample_)

                #np.savetxt("../Dataset/"+f,newdataset,delimiter=",")
                my_df = pd.DataFrame(newdataset)
                my_df.to_csv("../Dataset/"+f,header=True)

#getFiles("Dataset/DS1")

def aamiClassifying(x):
    if x in ('N','L','R','e','j'):
        return 'N'
    elif x in ('A','a','J','S'):
        return 'S'
    elif x in ('V','E'):
        return 'V'
    elif x in ('F'):
        return 'F'
    else:
        return 'Q'