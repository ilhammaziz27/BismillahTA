import sys
from numpy import NaN, Inf, arange, isscalar,asarray,array
import numpy as np
from matplotlib.pyplot import plot, scatter, show
import matplotlib.pyplot as plt

def normalisasi(x):
    normalized = (x-min(x))/(max(x)-min(x))
    return normalized
"""
#Peak Detection
def getSample(v,peak,start,end):
    return v[peak-start:peak+end]
"""
def exportToCSV(x,fname):
    np.savetxt(fname+".csv", x, delimiter=",")

def getSampleByRPeakDefined(X,rpeak,prev,after):
    samples = np.empty((0,250))

    if rpeak-prev < 0:
            return []
    elif rpeak+after > np.size(X):
            return []
    else:
            samples = X[rpeak-prev:rpeak+after+1]
    
    return samples
    
def getSample(x, m, n):
    RPeak,_ = peakdet(x,0.3)
    
    #samples = np.zeros((np.size(RPeak),250))
    samples = np.empty((0,250))
    
    j = 0
    for i in RPeak:
        if i[0]-m < 0:
            continue
        elif i[0]+n > np.size(x):
            break
        else:
            sample = x[int(i[0])-m:int(i[0])+(n+1)]
            samples = np.append(samples, [sample], axis=0)
        j=j+1
    
    return samples
    

def peakdet(v,delta,x=None):
    # Eli Billauer, 3.4.05 (Explicitly not copyrighted).
    # This function is released to the public domain; Any use is allowed.
    maxtab = []
    mintab = []

    if x is None:
        x = arange(len(v))

    v = asarray(v)

    if len(v) != len(x):
        sys.exit('Input vector v and x must have same length')

    if not isscalar(delta):
        sys.exit('Input argument delta must be a scalar')

    if delta<=0:
        sys.exit('Input argument delta must be positive')

    mn,mx, = Inf, -Inf
    mnpos,mxpos = NaN,NaN

    lookformax = True

    for i in arange(len(v)):
        this = v[i]
        if this > mx:
            mx = this
            mxpos = x[i]
        if this < mn:
            mn = this
            mnpos = x[i]

        if lookformax:
            if this < mx-delta:
                maxtab.append((mxpos,mx))
                mn = this
                mnpos = x[i]
                lookformax = False
        else:
            if this > mn+delta:
                mintab.append((mnpos,mn))
                mx = this
                mxpos = x[i]
                lookformax = True

    return array(maxtab),array(mintab)

def readSignal2(start,end):
    data = np.genfromtxt("../100.csv", delimiter=",", usecols=(1),skip_header=2)
    return data[start:end]

def readSignal(path):
    data = np.genfromtxt(path, delimiter=",", usecols=(1),skip_header=2)
    return data

def fetchData(path):
    data = np.genfromtxt(path, delimiter=",").astype(np.float32)
    return data[:,:250],data[:,250:254]

def plotSignal(data):
    #m=0
    #n=3000
    #maxtab, mintab = peakdet(data,.3)

    f, ax = plt.subplots(1, sharex=True)
    ax.plot(data)
    #ax.scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
    #ax.scatter(array(mintab)[:,0], array(mintab)[:,1], color='red')

    #j=0
    #for i in (maxtab):
     #   ax.annotate(maxtab[j,0],i)
      #  j=j+1

    #sample = getSample(data,2045,150,149)
    #ax[1].plot(data)

    f.subplots_adjust(hspace=0.3)
    #plt.show()