from __future__ import division,print_function,absolute_import

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import utils as ut
from sklearn import preprocessing
#matplotlib inline

learning_rate = 0.01
training_epochs = 20000
batch_size = 256
display_step = 1
examples_to_show = 10

#Network Parameters
n_input = 250
n_hidden_1 = 120
n_hidden_2 = 60
n_hidden_3 = 30
n_hidden_4 = 15
dataStart = 2000
dataEnd = 10000
datasetraw = ut.readSignal2(dataStart,dataEnd)
datasetraw_ = ut.normalisasi(datasetraw)
dataset_ = ut.getSample(datasetraw_,160,89)
dataset = dataset_

X = tf.placeholder("float",[None,n_input])

weights = {
    'encoder_h1': tf.Variable(tf.random_normal([n_input,n_hidden_1])),
    'encoder_h2': tf.Variable(tf.random_normal([n_hidden_1,n_hidden_2])),
    'encoder_h3': tf.Variable(tf.random_normal([n_hidden_2,n_hidden_3])),
    'encoder_h4': tf.Variable(tf.random_normal([n_hidden_3,n_hidden_4])),
    
    'decoder_h4': tf.Variable(tf.random_normal([n_hidden_4, n_hidden_3])),
    'decoder_h3': tf.Variable(tf.random_normal([n_hidden_3, n_hidden_2])),
    'decoder_h2': tf.Variable(tf.random_normal([n_hidden_2, n_hidden_1])),
    'decoder_h1': tf.Variable(tf.random_normal([n_hidden_1, n_input])),
}

biases = {
    'encoder_b1': tf.Variable(tf.random_normal([n_hidden_1])),
    'encoder_b2': tf.Variable(tf.random_normal([n_hidden_2])),
    'encoder_b3': tf.Variable(tf.random_normal([n_hidden_3])),
    'encoder_b4': tf.Variable(tf.random_normal([n_hidden_4])),
               
    'decoder_b4': tf.Variable(tf.random_normal([n_hidden_3])),
    'decoder_b3': tf.Variable(tf.random_normal([n_hidden_2])),
    'decoder_b2': tf.Variable(tf.random_normal([n_hidden_1])),
    'decoder_b1': tf.Variable(tf.random_normal([n_input])),
}

#Noising
def addNoise(x):
    #Noising dengan cara membuat 0 index tertentu secara random
    noise = np.random.choice([0,1], size=np.size(x), p=[1/4, 3/4])
    x_ = x*noise
    return x_

#Encoders
def encoder(x):
    layer_1 = tf.nn.sigmoid(tf.add(tf.matmul(addNoise(x),weights['encoder_h1']),biases['encoder_b1']))
    layer_2 = tf.nn.sigmoid(tf.add(tf.matmul(addNoise(layer_1),weights['encoder_h2']),biases['encoder_b2']))
    layer_3 = tf.nn.sigmoid(tf.add(tf.matmul(addNoise(layer_2),weights['encoder_h3']),biases['encoder_b3']))
    layer_4 = tf.nn.sigmoid(tf.add(tf.matmul(addNoise(layer_3),weights['encoder_h4']),biases['encoder_b4']))

    return layer_4

#Decoders
def decoder(x):
    layer_4 = tf.nn.sigmoid(tf.add(tf.matmul(x,tf.transpose(weights['encoder_h4'])),biases['decoder_b4']))
    layer_3 = tf.nn.sigmoid(tf.add(tf.matmul(layer_4, tf.transpose(weights['encoder_h3'])), biases['decoder_b3']))
    layer_2 = tf.nn.sigmoid(tf.add(tf.matmul(layer_3, tf.transpose(weights['encoder_h2'])), biases['decoder_b2']))
    layer_1 = tf.nn.sigmoid(tf.add(tf.matmul(layer_2, tf.transpose(weights['encoder_h1'])), biases['decoder_b1']))

    return layer_1

#Construct Model
encoder_op = encoder(X)
decoder_op = decoder(encoder_op)

#Prediction
x_new = encoder_op
y_pred = decoder_op
#Target
y_true = X

#Loss,optimizer, mse
cost = tf.reduce_mean(tf.pow(y_true - y_pred,2))
optimizer = tf.train.RMSPropOptimizer(learning_rate).minimize(cost)

init = tf.global_variables_initializer()


#Main
sess = tf.InteractiveSession()
sess.run(init)

c=1
epoch = 0
while (c > 0.01):
    _,c = sess.run([optimizer,cost], feed_dict={X: dataset})

    if epoch % display_step == 0:
        print("Epoch:",'%04d' % (epoch+1),
              "cost=", format(c))
    epoch = epoch + 1
print("Optimization finished")
encode_decode = sess.run(x_new,feed_dict={X: dataset})

ut.plotSignal(dataset[1])
ut.plotSignal(encode_decode[1])
ut.plt.show()