from __future__ import division,print_function,absolute_import

import tensorflow as tf
import numpy as np
import utils as ut
from sklearn.preprocessing import normalize
from AutoEncoder import AutoEncoder

class FullyConnected(object):
    def __init__(self,input_size,output_size,learning_rate,sess):
        self.__variables = {}
        self.__input_size = input_size
        self.__output_size = output_size
        self.__learning_rate = learning_rate

        self.__sess = sess
        self._setup_variables()

    @property
    def input_size(self):
        return self.__input_size

    @property
    def output_size(self):
        return self.__output_size

    @property
    def num_hidden_neurons(self):
        return self.__num_hidden_neurons

    @property
    def x(self):
        return self.__x

    @property
    def y_true(self):
        return self.__y_true

    @property
    def weights(self):
        return self.__weights

    @property
    def biases(self):
        return self.__biases

    @property
    def session(self):
        return self.__sess

    @property
    def learning_rate(self):
        return self.__learning_rate

    def _setup_variables(self):
        self.__x = tf.placeholder("float", [None, self.input_size])
        self.__y = tf.placeholder("float", [None, self.output_size])
        self.__y_true = tf.placeholder("float", [None, self.output_size])
        self.__weights = {
            'H1': tf.Variable(tf.random_normal([self.input_size, self.output_size]), trainable=True)
        }
        self.__biases = {
            'B1': tf.Variable(tf.random_normal([self.output_size]), trainable=True)
        }

    # Encoders
    def _feedForward(self,input):
        layer_1 = tf.nn.sigmoid(tf.add(tf.matmul(input, self.weights['H1']), self.biases['B1']))
        #layer_2 = tf.nn.sigmoid(tf.add(tf.matmul(layer_1, self.weights['H2']), self.biases['B2']))

        return layer_1

    def _predict(self,input):
        pred_for = self._feedForward(input)
        pred = tf.arg_max(pred_for,1)
        print(pred)

    def _train(self,input,label,threshold):
        self._setup_variables()

        #Predict
        y_pred = self._feedForward(self.x)

        # Loss,optimizer, mse
        cost = tf.reduce_mean(tf.pow(self.y_true - y_pred, 2))
        optimizer = tf.train.RMSPropOptimizer(self.learning_rate).minimize(cost)
        self.session.run(tf.global_variables_initializer())

        c = 1
        epoch = 1
        while (c >= threshold):
            feed_dict_train = {self.x: input, self.y_true: label}
            _, c = self.session.run([optimizer, cost], feed_dict=feed_dict_train)
            print(epoch, "MSE = ", c)
            epoch = epoch + 1

if __name__ == '__main__':
    x,y = ut.fetchData("../Dataset/Dataset2.csv")
    x_ = normalize(x,axis=1,norm='l1')

    sess = tf.Session()

    print("First Autoencoder")
    ae1 = AutoEncoder(250, 120, 0.1,"DAE1")
    ae1.train(x_, 0.00001)
    ae1.save_model()
    # a = ae1.encode(x_)
    # W, B = ae1.getHyperParameter()
    # ut.exportToCSV(W['encoder'], "DAE1W1")
    # ut.exportToCSV(W['decoder'], "DAE1W2")
    # ut.exportToCSV(B['encoder'], "DAE1B1")
    # ut.exportToCSV(B['decoder'], "DAE1B2")
    #
    # print("Second Autoencoder")
    # ae2 = AutoEncoder(125, 62, 0.1,"DAE2")
    # ae2.train(a[0], 0.0001)
    # b = ae2.encode(a[0])
    # W, B = ae1.getHyperParameter()
    # ut.exportToCSV(W['encoder'], "DAE2W1")
    # ut.exportToCSV(W['decoder'], "DAE2W2")
    # ut.exportToCSV(B['encoder'], "DAE2B1")
    # ut.exportToCSV(B['decoder'], "DAE2B2")

    # print("Third Autoencoder")
    # ae3 = AutoEncoder(60, 30, 0.01)
    # ae3.train(b[0], 0.01)
    # c = ae3.encode(b[0])



    # print("Fully Connected")
    # fc = FullyConnected(250,4,0.1,sess)
    # fc._train(x_,y,0.01)

    print("Optimization Done!")