from __future__ import division,print_function,absolute_import

import tensorflow as tf
import numpy as np
import utils as ut

class AutoEncoder(object):
    def __init__(self,input_size,num_hidden_neurons,learning_rate,model_name):
        self.__session = tf.Session()
        self.__variables = {}
        self.__input_size = input_size
        self.__num_hidden_neurons = num_hidden_neurons
        self.__learning_rate = learning_rate
        self.__model_name = model_name

        self.__setupVariables()

    @property
    def session(self):
        return self.__session

    @property
    def input_size(self):
        return self.__input_size

    @property
    def model_name(self):
        return self.__model_name

    @property
    def num_hidden_neurons(self):
        return self.__num_hidden_neurons

    @property
    def x(self):
        return self.__x

    @property
    def y_true(self):
        return self.__y_true

    @property
    def weights(self):
        return self.__weights

    @property
    def biases(self):
        return self.__biases

    @property
    def x_hat(self):
        return self.__x_hat

    @property
    def session(self):
        return self.__session

    @property
    def learning_rate(self):
        return self.__learning_rate

    def __setupVariables(self):
        self.__x = tf.placeholder("float", [None, self.input_size])
        self.__x_hat = []
        self.__y_true = tf.placeholder("float", [None, self.input_size])
        self.__weights = {
            'encoder': tf.Variable(tf.random_normal([self.input_size, self.num_hidden_neurons]), trainable=True),
            'decoder': tf.Variable(tf.random_normal([self.num_hidden_neurons, self.input_size]), trainable=True)
        }
        self.__biases = {
            'encoder': tf.Variable(tf.random_normal([self.num_hidden_neurons]), trainable=True),
            'decoder': tf.Variable(tf.random_normal([self.input_size]), trainable=True),
        }
        self.session.run(tf.global_variables_initializer())

    def getHyperParameter(self):
        return self.session.run([self.weights,self.biases])

    # Noising
    def __addNoise(self, input):
        # Noising dengan cara membuat 0 index tertentu secara random
        noise = np.random.choice([0, 1], size=np.size(input), p=[1 / 4, 3 / 4])
        return input * noise

    # Encoders
    def __encoder(self, input):
        encoded = tf.nn.sigmoid(tf.add(tf.matmul(input, self.weights['encoder']), self.biases['encoder']))
        return encoded

    # Decoders
    def __decoder(self, input):
        decoded = tf.nn.sigmoid(tf.add(tf.matmul(input, self.weights['decoder']), self.biases['decoder']))
        return decoded

    def encode(self,input):
        feed_dict = {self.x: input}
        encoder = self.__encoder(self.x)

        return self.session.run([encoder], feed_dict=feed_dict)

    def train(self, input, threshold):
        #self.__setupVariables()
        x_hat = self.__encoder(self.__addNoise(self.x))
        decoder_op = self.__decoder(x_hat)

        # Prediction
        y_pred = decoder_op

        # Loss,optimizer, mse
        cost = tf.reduce_mean(tf.pow(self.y_true - y_pred, 2))
        optimizer = tf.train.RMSPropOptimizer(self.learning_rate).minimize(cost)
        self.session.run(tf.global_variables_initializer())

        c = 1
        epoch = 1
        while (c >= threshold):
            feed_dict_train = {self.x: input, self.y_true: input}
            _, c = self.session.run([optimizer, cost], feed_dict=feed_dict_train)
            print(self.model_name, "epoch : ", epoch, " | MSE = ", c)
            epoch = epoch + 1

    def save_model(self):
        saver = tf.train.Saver()
        #self.session.run(tf.global_variables_initializer())
        saver.save(self.session, self.model_name, global_step=1000)

    def load_model(self,file):
        new_saver = tf.train.import_meta_graph(file)
        new_saver.restore(self.session, tf.train.latest_checkpoint('./'))
        print(self.session.run())

if __name__ == '__main__':
    datasetraw = ut.readSignal("dataset/datatrain/100.csv")
    datasetraw_ = ut.normalisasi(datasetraw)
    dataset_ = ut.getSample(datasetraw_, 160, 89)
    dataset = dataset_

    print("First Autoencoder")
    ae1 = AutoEncoder(250,120,0.01)
    ae1.train(dataset, 0.03)
    a = ae1.encode(dataset)

    print("Second Autoencoder")
    ae2 = AutoEncoder(120, 60, 0.01)
    ae2.train(a[0], 0.01)
    b = ae2.encode(a[0])

    print("Third Autoencoder")
    ae3 = AutoEncoder(60, 30, 0.01)
    ae3.train(b[0], 0.01)
    c = ae3.encode(b[0])

    print(c)

    print("Optimization Done!")